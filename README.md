### Pipeline Staging
[![pipeline status](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/-/commits/staging) [![coverage report](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/-/commits/staging)

### Pipeline Master

[![pipeline status](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/badges/master/pipeline.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/-/commits/master) [![coverage report](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/badges/master/coverage.svg)](https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006463162-Immanuel/a7-anagram-margana/websocket-service-anagram-margana/-/commits/master)

<br>


### Dokumentasi:

[DOCUMENTATION.md](docs/DOCUMENTATION.md)



## Sprint 2

### Added

- Fungsionalitas untuk meneruskan pesan client ke main-service
- Fungsionalitas untuk meneruskan pesan main-service ke client

### Changed

### Fixed




## Sprint 3

### Added
- fitur heartbeat untuk mendeteksi apabila seseorang disconnect ataupun connect
### Changed
### Fixed
- client tidak bisa mengirim data yang bukan string
