## Kegunaan

Microservice yang bertujuan untuk meminimalkan
latensi koneksi antara client-server, khususnya mengenai kasus-kasus
yang sangat membutuhkan real-time communication.
<br/><br/>


## Mekanisme

Semua komunikasi akan menggunakan format json. Service ini bekerja dengan cara menjadi penengah antara
pemain (client) dan server utama (main service). Fitur-fitur pada front-end yang membutuhkan konektivitas real-time akan
memanfaatkan protokol komunikasi websocket yang terhubung ke Service ini.  Kemudian segala komunikasi websocket yang telah dikirimkan client melalui websocket tersebut akan diteruskan ke main service melalui REST API. Data yang dikembalikan oleh REST API main service tersebut akan diteruskan kembali ke client. Main service juga bisa secara inisiatif mengirimkan data ke REST API service ini, di mana data tersebut akan diteruskan kembali ke client melalui websocket.

![microservice diagram](microservice.jpg)

### inisialisasi

Ketika client ingin menghubungkan websocket, client perlu mengirimkan *connect http request* ke endpoint  `/anagram-margana-websocket`. Setelah selesai connect, client perlu mengirimkan sebuah json ke `/init` 
dengan data berikut: <br/>

- roomCode (String)
- playerId (long)
- sessionToken (String)
- websocketServiceAddress (string)

Note: <br>
websocketServiceAddress merupakan alaman websocket service yang mengirimkan request tersebut

Data ini akan diteruskan ke REST API pada main-site `/communication-api/init` 
sebagai POST method. Apabila main-site mengembalikan `1`, maka artinya autentikasi diterima. Apabila `0` , maka artinya autentikasi ditolak

Selain connect, client juga akan meng-subscribe websocket `/subscribe/{playerId}/{sessionToken}`

<br />

### komunikasi client-to-server

Untuk komunikasi selanjutnya, client akan mengirimkan json dengan key: <br />
- type (String): tujuan dari pesan tersebut (misal `submit-answer` atau `vote-restart`).
- playerId (long)
- sessionToken (String)
- body (Any): konten dari pesan tersebut


Data tersebut akan diteruskan ke main-site dengan ketentuan:
- URL: `/communication-api/msg-from-client/{type}`
- Method: POST
- content (json):
  - roomCode
  - playerId
  - sessionToken
  - body

Setelah mengirim POST ke main-site, apabila response yang dikembalikan oleh main-site 
bukan string kosong dan merupakan json yang valid (terdiri atas `type` dan `body`), 
maka server websocket ini akan mengirim message balasan ke client 
sesuai response dari main-site tersebut. 

catatan: <br>`type ` sebaiknya sebaiknya hanya boleh berisi string dengan karakter `a` sampai `z`, `0` sampai `9`, atau `-`.

<br>

### komunikasi server-to-client

Ketika main-site ingin mengirimkan message ke client tertentu,
main-site akan mengirimkannya ke server websocket dengan ketentuan berikut:
- URL: `/from-main-site`
- Method: POST
- content (json):
  - playerId
  - sessionToken
  - type
  - body

Service ini akan memberikan response berupa `-1` 
apabila autentikasi ditolak (misal salah playerId atau salah sessionToken), dan `1` ketika berhasil. 

Setelah itu, data-data tersebut akan diteruskan ke client dengan 
alamat websocket sesuai dengan alamat yang di-subscribe 
oleh client (lihat [section inisialisasi di atas](#inisialisasi)). Data-data yang dikirim
merupakan json sebagai berikut:

- type
- body

<br><br>

Catatan: sessionToken dikirimkan ketika 
main-site menghubungi server websocket (dan sebaliknya) 
untuk menjaga keamanan. Hal ini didasarkan pada fakta bahwa normalnya hanya main service, websocket service, dan client yang mengetahui data tersebut.


### heartbeat

Heartbeat merupakan suatu fitur untuk mendeteksi apakah suatu pemain masih 
terhubung di dalam permainan atau tidak. 

Ketika ada pemain yang terputus,
service ini akan mengirimkan message ke main-service pada endpoint 
`/communication-api/msg-from-client/disconnected`.

Ketika ada pemain yang terhubung,
service ini akan mengirimkan message ke main-service pada endpoint
`/communication-api/msg-from-client/connected`.

