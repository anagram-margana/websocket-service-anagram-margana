package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication;

import lombok.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class AnagramMarganaClientServerCommunicationApplication {
    
    @Generated
    public static void main(String[] args) {
        SpringApplication.run(AnagramMarganaClientServerCommunicationApplication.class, args);
    }
    
}
