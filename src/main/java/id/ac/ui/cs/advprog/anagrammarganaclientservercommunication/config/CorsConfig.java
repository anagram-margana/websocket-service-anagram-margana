package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.config;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.Constants;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Untuk setting CORS supaya allow datangnya request dari main site.
 * Konfigurasi di sini tidak berpengaruh untuk websocket. Hanya http saja.
 */

@EnableWebMvc
@Configuration
@ComponentScan
public class CorsConfig implements WebMvcConfigurer {
    String mainServerAddress = Constants.getMainServerAddress().toString();
    
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins(mainServerAddress);
    }
}
