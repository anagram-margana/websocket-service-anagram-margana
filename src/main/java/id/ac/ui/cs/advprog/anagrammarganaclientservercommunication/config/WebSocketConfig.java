package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.config;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.Constants;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;


@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    String mainServerAddress  = Constants.getMainServerAddress().toString();
    
    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry){
        stompEndpointRegistry
                // Handshake endpoint
                .addEndpoint("/anagram-margana-websocket")
                
                // Allow dari main-site
                .setAllowedOrigins(mainServerAddress)
                .withSockJS();
    }
    
}
