package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.AuthenticationService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.SendMessageToClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainSiteMessageToClientController {
    Logger logger = LoggerFactory.getLogger("MainSiteToClient");
    
    @Autowired
    AuthenticationService authenticationService;
    
    @Autowired
    SendMessageToClientService sendMessageToClientService;
    
    
    /**
     * Required data: <br>
     * {@link MessageDTO#getPlayerId()} <br>
     * {@link MessageDTO#getSessionToken()} <br>
     * {@link MessageDTO#getType()} <br>
     * {@link MessageDTO#getBody()} <br>
     */
    @PostMapping(value = "/from-main-site", consumes = "application/json")
    public ResponseEntity<String> handleRequest(
            @RequestBody MessageDTO<Object> messageDto) throws JsonProcessingException {
        var isAuthenticated = authenticationService.authenticateByUsingRepository(messageDto);
        if (!isAuthenticated)
            return ResponseEntity.ok("-1");
        
        var isSuccess = sendMessageToClientService.sendMessage(messageDto);
        
        if (isSuccess)
            return ResponseEntity.ok("1");
        return ResponseEntity.ok("0");
    }
}
