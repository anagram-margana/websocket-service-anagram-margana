package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller;


import lombok.Generated;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;



/**
 * Just a dummy controller that showing "Websocket service is running"
 * to inidicate that it is deployed successfully
 */
@Generated
@Controller
public class RunningStatusController {
    @GetMapping({"/", ""})
    public ResponseEntity<String> websiteIsRunning(){
        return ResponseEntity.ok("Websocket app is running");
    }
}
