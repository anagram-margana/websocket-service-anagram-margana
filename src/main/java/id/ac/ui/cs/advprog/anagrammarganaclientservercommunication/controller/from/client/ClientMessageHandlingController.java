package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller.from.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.ClientMessageToMainSiteService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.JsonParsingUtility;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@RestController
public class ClientMessageHandlingController {
    @Autowired
    ClientMessageToMainSiteService clientMessageToMainSiteService;
    
    @Setter
    Logger logger = LoggerFactory.getLogger("pass-msg-to-main-site");
    
    
    @MessageMapping(value = "/from-client")
    public void handleMessage(Message<String> message) throws JsonProcessingException {
        logger.info("Incoming msg from client: {}", message);
        
        // converting message content (json) into a hashmap
        var util = new JsonParsingUtility();
        var messageContent = message.getPayload();
        var hashmap = util.jsonAsHashmap(messageContent);
    
        var body = hashmap.get("body");
        var type = (String) hashmap.get("type");
        var playerIdAsStr = (String) hashmap.get("playerId");
        var playerId = Long.parseLong(playerIdAsStr);
        var sessionToken = (String) hashmap.get("sessionToken");
        
        if (sessionToken == null) {
            logger.error("null sessionToken for data: {}", messageContent);
            return;
        }
        
        try{
            clientMessageToMainSiteService.handleMessage(playerId, sessionToken, type, body);
            
        }catch (WebClientResponseException e){
            var errMessage =
                    String.format("error type: %s playerId: %s sessionToken: %s body: %s \n" +
                                          "error-reason: %s \n response body: %s",
                                  type, playerId, sessionToken, body, e.getMessage(), e.getResponseBodyAsString());
            logger.error(errMessage, e);
        }catch (IllegalStateException e){
            logger.info(e.getMessage());
        }
    }
}
