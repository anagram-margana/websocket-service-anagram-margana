package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller.from.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.AuthenticationService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.HeartBeatHandlingService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class HeartBeatHandlingController {
    @Autowired
    AuthenticationService authenticationService;
    
    @Autowired
    HeartBeatHandlingService heartBeatHandlingService;
    
    @MessageMapping(value = "/heartbeat")
    @SneakyThrows
    public void handleIncomingHeartbeat(Message<String> msg) {
        var content = msg.getPayload();
        var objectMapper = new ObjectMapper();
        
        var playerIdentity = objectMapper.readValue(content, PlayerIdentity.class);
        var isAuthenticated = authenticationService.authenticateByUsingRepository(playerIdentity);
        if (!isAuthenticated)
            return;
        
        heartBeatHandlingService.refreshOnlineStatus(playerIdentity.getPlayerId());
    }
}
