package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller.from.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class InitialConnectionController {
    Logger logger = LoggerFactory.getLogger("websocket-init");
    
    @Autowired
    AuthenticationService authenticationService;
    
    
    @MessageMapping(value = "/init")
    public void handleInitRequest(Message<String> msg) throws JsonProcessingException{
        var content = msg.getPayload();
        var objectMapper = new ObjectMapper();
        
        var playerIdentity = objectMapper.readValue(content, PlayerIdentity.class);
        var isAuthenticated = authenticationService.authenticateToServer(playerIdentity);
        
        if (isAuthenticated){
            authenticationService.savePlayerIdentity(playerIdentity);
            logger.info("Player with ID {} successfully send init message",  playerIdentity);
        }else{
            logger.info("Player with ID {} init message FAIL", playerIdentity);
        }
    }
}
