package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;


/**
 * Can be used to passing message from both client-to-main-site or main-site-to-client.
 * @param <T> the type of the body
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageDTO<T> {
    long playerId;
    
    @Nullable
    String roomCode;
    
    @Nullable
    String sessionToken;
    
    String type;
    
    T body;
}
