package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Ignore;


@Data
@Ignore
@NoArgsConstructor
@AllArgsConstructor
public class PlayerIdentity {
    long playerId;
    String roomCode;
    String sessionToken;
}
