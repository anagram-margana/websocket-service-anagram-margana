package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

@Repository
public class PlayerIdentityRepository {
    Cache<Long, PlayerIdentity> playerIdMapping;
    
    @PostConstruct
    public void init(){
        playerIdMapping = CacheBuilder.newBuilder()
                .expireAfterAccess(4, TimeUnit.HOURS)
                .build();
    }
    
    public void add(PlayerIdentity playerIdentity){
        var playerId = playerIdentity.getPlayerId();
        playerIdMapping.put(playerId, playerIdentity);
    }
    
    public PlayerIdentity findByPlayerIdOrNull(long playerId){
        return playerIdMapping.getIfPresent(playerId);
    }
    
    public Collection<PlayerIdentity> findAll(){
        return playerIdMapping.asMap().values();
    }
}
