package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository;

import com.google.common.base.Ticker;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import lombok.Setter;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class PlayerOnlineStatusRepository {
    Cache<Long, PlayerIdentity> playerIdMapping;
    List<RemovalListener<Long, PlayerIdentity>> removalListeners;
    
    @Setter
    Ticker ticker = null;
    
    @PostConstruct
    public void init(){
        removalListeners = new ArrayList<>();
        setExpirationTimeout(12, TimeUnit.SECONDS);
    }
    
    public long getSize(){
        playerIdMapping.cleanUp();
        return playerIdMapping.size();
    }
    
    public void setExpirationTimeout(long duration, TimeUnit timeUnit){
        if (playerIdMapping != null) {
            playerIdMapping.cleanUp();
            if (playerIdMapping.size() > 0)
                throw new IllegalStateException("Repository is not empty");
        }
        
        var builder = CacheBuilder.<Long, PlayerIdentity>newBuilder()
                .expireAfterAccess(duration, timeUnit)
                .removalListener(this::internalRemovalListener);
        
        if (ticker != null)
            builder.ticker(ticker);
        playerIdMapping = builder.build();
    }
    
    public boolean isOnline(PlayerIdentity playerIdentity){
        var playerId = playerIdentity.getPlayerId();
        var asHashmap = playerIdMapping.asMap();
        return asHashmap.containsKey(playerId);
    }
    
    public void refreshOnlineStatus(PlayerIdentity playerIdentity){
        var playerId = playerIdentity.getPlayerId();
        playerIdMapping.put(playerId, playerIdentity);
    }
    
    private void internalRemovalListener(RemovalNotification<Object, Object> notificationObjectObject){
        playerIdMapping.cleanUp();
        
        Object raw = notificationObjectObject;
        var notification = (RemovalNotification<Long, PlayerIdentity>) raw;
        for (var listener: removalListeners) {
            listener.onRemoval(notification);
        }
    }
    
    public void addRemovalListener(RemovalListener<Long, PlayerIdentity> listener){
        removalListeners.add(listener);
    }
}
