package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.Constants;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSenderFactory;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.RequestSenderFactory;
import lombok.Setter;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.util.Map;

@Service
public class AuthenticationService {
    URL thisWebsocketServiceUrlAddress = Constants.getWebsocketServerDefaultAddress();
    
    @Autowired
    PlayerIdentityRepository playerIdentityRepository;
    
    IRequestSenderFactory requestSenderFactory;
    URL mainSiteUrl = Constants.getMainServerAddress();
    
    @Setter
    Logger logger = LoggerFactory.getLogger("websocket-auth");
    
    @PostConstruct
    public void init(){
        requestSenderFactory = new RequestSenderFactory();
    }
    public void init(URL mainSiteUrl, IRequestSenderFactory requestSenderFactory, URL thisWebsocketServiceUrlAddress){
        this.mainSiteUrl = mainSiteUrl;
        this.requestSenderFactory = requestSenderFactory;
        this.thisWebsocketServiceUrlAddress = thisWebsocketServiceUrlAddress;
    }
    
    
    
    /**
     * Mengecek autentikasi identitas suatu client dengan memanggil REST API dari main-site.
     * @param playerIdentity identitas yang ingin di-cek kebenarannya
     * @return boolean true jika autentikasi valid, dan false jika sebaliknya.
     */
    @SneakyThrows
    public boolean authenticateToServer(PlayerIdentity playerIdentity) {
        var roomCode = playerIdentity.getRoomCode();
        var playerIdAsStr = Long.toString(playerIdentity.getPlayerId());
        var sessionToken = playerIdentity.getSessionToken();
        
        var mainSiteInitEndpointStr = "/communication-api/init";
        var mainSiteInitEndpoint = new URL(mainSiteUrl, mainSiteInitEndpointStr);

        var formData = Map.of(
                "roomCode", roomCode,
                "playerId", playerIdAsStr,
                "sessionToken", sessionToken,
                "websocketServiceAddress", thisWebsocketServiceUrlAddress.toString()
        );
        
        var requestSender = requestSenderFactory.createRequestSender(mainSiteInitEndpoint);
        String response = requestSender.sendPost(formData, String.class);

        if (response.equals("1")) {
            return true;
        }else if (response.equals("0") || response.equals("-1"))
            return false;

        logger.error("Illegal response value: `{}` from main-site {} for client identity {}",
                     response,
                     mainSiteInitEndpointStr,
                     playerIdentity);
        return false;
    }
    
    public boolean authenticateByUsingRepository(PlayerIdentity player){
        return authenticateByUsingRepository(player.getPlayerId(), player.getSessionToken());
    }
    
    public <T> boolean authenticateByUsingRepository(MessageDTO<T> messageDTO){
        if (messageDTO.getRoomCode() == null) {
            return authenticateByUsingRepository(messageDTO.getPlayerId(), messageDTO.getSessionToken());
        }
        return authenticateByUsingRepository(messageDTO.getPlayerId(),
                                             messageDTO.getRoomCode(),
                                             messageDTO.getSessionToken());
    }
    
    public boolean authenticateByUsingRepository(long playerId, String roomCode, String sessionToken){
        if (!authenticateByUsingRepository(playerId, sessionToken))
            return false;
        var player = playerIdentityRepository.findByPlayerIdOrNull(playerId);
        var ret = player.getRoomCode().equals(roomCode);
        if (ret)
            logger.info("Invalid room code. Id: {}, roomCode: {}", playerId, roomCode);
        return ret;
    }
    
    public boolean authenticateByUsingRepository(long playerId, String sessionToken){
        var player = playerIdentityRepository.findByPlayerIdOrNull(playerId);
        if (player == null) {
            logger.info("Invalid player id {}", playerId);
            return false;
        }
        var ret = player.getSessionToken().equals(sessionToken);
        if (!ret)
            logger.info("Invalid player sessionToken. Id: {} sessionToken: {}", playerId, sessionToken);
        return ret;
    }
    
    public void savePlayerIdentity(PlayerIdentity playerIdentity){
        playerIdentityRepository.add(playerIdentity);
    }
}
