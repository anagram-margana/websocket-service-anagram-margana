package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.Constants;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSender;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSenderFactory;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.RequestSenderFactory;
import lombok.Setter;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@Service
public class ClientMessageToMainSiteService {
    @Setter
    URL mainServerAddress = Constants.getMainServerAddress();
    
    Level debugInfo = Level.forName("DebugInfo", 450);
    
    @Autowired
    PlayerIdentityRepository playerIdentityRepository;
    
    @Autowired
    AuthenticationService authenticationService;
    
    @Autowired
    SendMessageToClientService sendMessageToClientService;
    
    @Autowired
    HeartBeatHandlingService heartBeatHandlingService;
    
    @Setter
    IRequestSenderFactory requestSenderFactory = new RequestSenderFactory();
    
    @Setter
    Logger logger = LogManager.getLogger(ClientMessageToMainSiteService.class);

    
    
    public <T> void handleMessage(long playerId, String sessionToken,
                              String type, T message){
        // authentication
        var player = playerIdentityRepository.findByPlayerIdOrNull(playerId);
        var roomCode = (player != null)? player.getRoomCode() : null;
        
        var isAuthenticated = authenticationService.authenticateByUsingRepository(playerId, sessionToken);
        if (!isAuthenticated || player == null || roomCode == null) {
            var msg = String.format("Fail authentication for id: %s, room: %s, player: %s",
                                    playerId, roomCode,  player);
            throw new IllegalStateException(msg);
        }
    
        heartBeatHandlingService.refreshOnlineStatus(playerId);
        var response = sendMessageToMainSite(playerId, roomCode, sessionToken, type, message);
        if (response != null)
            replyBackToClient(response);
   
    }
    
    
    public <T> MessageDTO<Object> sendMessageToMainSite(long playerId, String roomCode,
                                                        String sessionToken, String type, T message) {
        try {
            var mainSiteHandlerForClientMsg =
                    new URL(mainServerAddress, "/communication-api/msg-from-client/" + type);
            var requestSender = requestSenderFactory.createRequestSender(mainSiteHandlerForClientMsg);
            return sendMessageToMainSiteWithRequestSender(playerId, roomCode, sessionToken,
                                                          message, requestSender);
        } catch (MalformedURLException e) {
            logger.error("Unexpected MalformedURL", e);
        }
        return null;
    }
    
    
    public <T> MessageDTO<Object> sendMessageToMainSiteWithRequestSender(long playerId, String roomCode,
                                                                         String sessionToken, T message,
                                                                         IRequestSender requestSender){
        Object requestData = Map.of(
                "roomCode", roomCode,
                "playerId", Long.toString(playerId),
                "sessionToken", sessionToken,
                "body", message
        );
        
        var referenceType = new ParameterizedTypeReference<MessageDTO<Object>>(){};
        var responseFromMainSite = requestSender.sendPost(requestData, referenceType);
        logger.log(debugInfo, "Response from main site: {}", responseFromMainSite);
        return responseFromMainSite;
    }
    
    
    public void replyBackToClient(MessageDTO<Object> message) {
        try {
            sendMessageToClientService.sendMessage(message);
        } catch (JsonProcessingException e) {
            logger.error("", e);
        }
    }
}
