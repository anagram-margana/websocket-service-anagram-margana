package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import com.google.common.cache.RemovalNotification;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerOnlineStatusRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class HeartBeatHandlingService {
    @Autowired
    PlayerOnlineStatusRepository playerOnlineStatusRepository;
    
    @Autowired
    PlayerIdentityRepository playerIdentityRepository;
    
    @Autowired
    ClientMessageToMainSiteService clientMessageToMainSiteService;
    
    Logger logger = LogManager.getLogger(HeartBeatHandlingService.class);
    
    @PostConstruct
    public void init(){
        playerOnlineStatusRepository.addRemovalListener(this::removalListener);
    }
    
    public void triggerCleanUp(){
        playerOnlineStatusRepository.getSize();
    }
    
    public void refreshOnlineStatus(long playerId){
        // reset its cache timeout duration
        var playerIdentity = playerIdentityRepository.findByPlayerIdOrNull(playerId);
        
        var roomCode = playerIdentity.getRoomCode();
        var sessionToken = playerIdentity.getSessionToken();
        
        var isPreviouslyOnline = playerOnlineStatusRepository.isOnline(playerIdentity);
        
        playerOnlineStatusRepository.refreshOnlineStatus(playerIdentity);
        if (!isPreviouslyOnline)
            clientMessageToMainSiteService.sendMessageToMainSite(playerId, roomCode, sessionToken,
                                                                 "connected", "");
    }
    
    private void removalListener(RemovalNotification<Long, PlayerIdentity> notification){
        var player = notification.getValue();
        var playerId = player.getPlayerId();
        var roomCode = player.getRoomCode();
        var sessionToken = player.getSessionToken();
        
        logger.info("Online Status {} {}", playerId, notification.wasEvicted());
        
        if (!notification.wasEvicted())
            return;
        
        clientMessageToMainSiteService.sendMessageToMainSite(playerId, roomCode, sessionToken,
                                                             "disconnected", "");
    }
}
