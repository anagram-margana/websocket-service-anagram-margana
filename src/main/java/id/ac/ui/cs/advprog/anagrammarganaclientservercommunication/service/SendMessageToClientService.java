package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class SendMessageToClientService {
    @Autowired
    SimpMessagingTemplate messagingTemplate;
    
    @Autowired
    PlayerIdentityRepository playerIdentityRepository;
    
    
    public <T> boolean sendMessage(MessageDTO<T> messageDTO) throws JsonProcessingException{
        var playerId = messageDTO.getPlayerId();
        var sessionToken = messageDTO.getSessionToken();
        var type = messageDTO.getType();
        var body = messageDTO.getBody();
        
        return sendMessage(playerId,
                           sessionToken,
                           type, body);
    }
    
    public <T> boolean sendMessage(long playerId, String sessionToken, String type, T body) throws JsonProcessingException {
        var objectMapper = new ObjectMapper();
        
        var wrappedData = new HashMap<String, Object>();
        wrappedData.put("type", type);
        wrappedData.put("body", body);
        
        var wrappedDataJson = objectMapper.writeValueAsString(wrappedData);
        
        var destination = String.format("/subscribe/%s/%s",
                                        playerId, sessionToken);
        
        messagingTemplate.convertAndSend(destination, wrappedDataJson);
        return true;
    }
}
