package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import org.springframework.core.ParameterizedTypeReference;

import java.net.URL;
import java.util.Map;

public interface IRequestSender {
    
    URL getDestination();
    <T> T sendPost(Map<String, String> formData, Class<T> returnType);
    <T> T sendPost(Map<String, String> formData, ParameterizedTypeReference<T> returnType);
    <T> T sendPost(Object data, Class<T> returnType);
    <T> T sendPost(Object data, ParameterizedTypeReference<T> returnType);
    
}
