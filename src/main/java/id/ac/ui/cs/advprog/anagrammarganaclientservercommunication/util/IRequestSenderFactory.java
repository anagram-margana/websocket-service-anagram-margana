package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;

public interface IRequestSenderFactory {
    IRequestSender createRequestSender(URL destination);
    
    IRequestSender createRequestSender(URL destination, WebClient webClient);
}
