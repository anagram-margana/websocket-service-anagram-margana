package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class JsonParsingUtility {
    public <T> Map<String, T> jsonAsHashmap(String json) throws JsonProcessingException {
        var objectMapper = new ObjectMapper();
        var typeReference = new TypeReference<HashMap<String,T>>() {};
        return objectMapper.readValue(json, typeReference);
    }
}
