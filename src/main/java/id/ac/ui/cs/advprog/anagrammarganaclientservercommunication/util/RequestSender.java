package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;
import java.util.Map;

public class RequestSender implements IRequestSender {
    URL destination;
    WebClient webClient;
    
    public RequestSender(URL destination){
        this(destination, WebClient.create(destination.toString()));
    }
    
    public RequestSender(URL destination, WebClient webClient){
        this.destination = destination;
        this.webClient = webClient;
    }
    
    @Override
    public URL getDestination() {
        return destination;
    }
    
    @Override
    public <T> T sendPost(Map<String, String> formData, Class<T> returnType){
        return sendPostWithoutSpecificReturnType(formData)
                .bodyToMono(returnType)
                .block();
    }
    
    @Override
    public <T> T sendPost(Map<String, String> formData, ParameterizedTypeReference<T> returnType){
        return sendPostWithoutSpecificReturnType(formData)
                .bodyToMono(returnType)
                .block();
    }
    
    @Override
    public <T> T sendPost(Object data, Class<T> returnType){
        return sendPostWithoutSpecificReturnType(data)
                .bodyToMono(returnType)
                .block();
    }
    
    @Override
    public <T> T sendPost(Object data, ParameterizedTypeReference<T> returnType){
        return sendPostWithoutSpecificReturnType(data)
                .bodyToMono(returnType)
                .block();
    }
    
    
    protected WebClient.ResponseSpec sendPostWithoutSpecificReturnType(Map<String, String> formData){
        var bodyValues = new LinkedMultiValueMap<String, String>();
        for (var formEntry: formData.entrySet()) {
            var value = formEntry.getValue();
            bodyValues.add(formEntry.getKey(), value);
        }
    
        return webClient
                .post()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData(bodyValues))
                .retrieve();
    }
    
    
    protected WebClient.ResponseSpec sendPostWithoutSpecificReturnType(Object messageData){
        
        return webClient
                .post()
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(messageData))
                .retrieve();
    }
}
