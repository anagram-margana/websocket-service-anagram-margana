package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;

public class RequestSenderFactory implements IRequestSenderFactory {
    
    @Override
    public IRequestSender createRequestSender(URL destination) {
        return new RequestSender(destination);
    }
    
    @Override
    public IRequestSender createRequestSender(URL destination, WebClient webClient) {
        return new RequestSender(destination, webClient);
    }
}
