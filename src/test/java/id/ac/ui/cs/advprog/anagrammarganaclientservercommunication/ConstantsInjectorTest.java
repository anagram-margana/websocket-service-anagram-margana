package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ConstantsInjectorTest {
    static URL originalMainSiteUrl;
    static URL originalWebsocketUrl;
    String arbitraryUrlPath1 = "http://localhost:1234";
    String arbitraryUrlPath2 = "http://localhost:3456";
    URL arbitraryUrl1 = new URL(arbitraryUrlPath1);
    URL arbitraryUrl2 = new URL(arbitraryUrlPath2);
    
    ConstantsInjectorTest() throws MalformedURLException {}
    
    
    @BeforeAll
    static void setUp() {
        originalMainSiteUrl = Constants.getMainServerAddress();
        originalWebsocketUrl = Constants.getWebsocketServerDefaultAddress();
    }
    
    @AfterAll
    static void tearDown() {
        Constants.setMainServerAddress(originalMainSiteUrl);
        Constants.setWebsocketServerDefaultAddress(originalWebsocketUrl);
    }
    
    @Test
    void setMainServerAddress() throws MalformedURLException {
        var constantInjector = new ConstantsInjector();
        
        assertNotEquals(arbitraryUrl1, Constants.getMainServerAddress());
        constantInjector.setMainServerAddress(arbitraryUrlPath1);
        assertEquals(arbitraryUrl1, Constants.getMainServerAddress());
    }
    
    @Test
    void setWebsocketServerAddress() throws MalformedURLException {
        var constantInjector = new ConstantsInjector();
    
        assertNotEquals(arbitraryUrl2, Constants.getWebsocketServerDefaultAddress());
        constantInjector.setWebsocketServerAddress(arbitraryUrlPath2);
        assertEquals(arbitraryUrl2, Constants.getWebsocketServerDefaultAddress());
    }
}