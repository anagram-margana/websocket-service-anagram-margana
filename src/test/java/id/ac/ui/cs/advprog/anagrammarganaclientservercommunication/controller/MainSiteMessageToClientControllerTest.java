package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.AuthenticationService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.SendMessageToClientService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc()
class MainSiteMessageToClientControllerTest {
    @LocalServerPort
    private int port;
    private String URL;
    
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    AuthenticationService authenticationService;
    
    @MockBean
    SendMessageToClientService sendMessageToClientService;
 
    
    @BeforeEach
    public void setup(){
        MockitoAnnotations.openMocks(this);
        URL = "http://localhost:" + port + "/anagram-margana-websocket";
    }
    
    
    @Test
    void handleMessageTest() {
        handleMessageTest(true, true, "1");
    }
    
    
    @Test
    void handleMessageTest_returnNegativeWhenNotAuthenticated() {
        handleMessageTest(false, true, "-1");
    }
    
    @Test
    void handleMessageTest_returnZeroWhenFail() {
        handleMessageTest(true, false, "0");
    }
    
    @SneakyThrows
    private void handleMessageTest(boolean authenticationServiceReturnValue,
                                   boolean sendMessageToClientServiceReturnValue,
                                   String expectedResponse){
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
    
        var formDataDTO = new MessageDTO<String>();
        formDataDTO.setPlayerId(1);
        formDataDTO.setType("test-mockito");
        formDataDTO.setRoomCode("ABC");
        formDataDTO.setSessionToken("Token");
        formDataDTO.setBody("idk");
    
        when(authenticationService.authenticateByUsingRepository(formDataDTO))
                .thenReturn(authenticationServiceReturnValue);
        when(sendMessageToClientService.sendMessage(formDataDTO))
                .thenReturn(sendMessageToClientServiceReturnValue);
    
        var objectMapper = new ObjectMapper();
        var formDataJson = objectMapper.writeValueAsString(formDataDTO);
    
        mockMvc.perform(post("/from-main-site")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(formDataJson))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }
    
    
    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }
}