package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller.from.client;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.ClientMessageToMainSiteService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.HeartBeatHandlingService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.ResourceReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.Message;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
class ClientMessageHandlingControllerTest {
    @MockBean
    ClientMessageToMainSiteService clientMessageToMainSiteService;
    
    
    @Autowired
    @InjectMocks
    ClientMessageHandlingController clientMessageHandlingController;
    
    @Mock
    Message<String> messageMock;
    long playerId = 107;
    String type=  "testing";
    String sessionToken = "asdfghjkl";
    String body = "content";
    
    Logger logger;
    
    
    @BeforeEach
    public void setup() {
        clientMessageToMainSiteService = Mockito.mock(ClientMessageToMainSiteService.class,
                                                      Mockito.withSettings()
                                                     .useConstructor()
                                                     .defaultAnswer(Answers.CALLS_REAL_METHODS));
        MockitoAnnotations.openMocks(this);
        
        logger = mock(Logger.class);
        clientMessageHandlingController.setLogger(logger);
    }
    
    
    @Test
    void handleMessageRunsWithoutException() throws Exception {
        var jsonPath = "testing-resources/controller/ClientMessageHandlingControllerTest/MessagePayload.json";
        var json = ResourceReader.readAsStr(jsonPath);
        when(messageMock.getPayload()).thenReturn(json);
        doNothing()
                .when(clientMessageToMainSiteService)
                .handleMessage(playerId, sessionToken, type, body);
    
        clientMessageHandlingController.handleMessage(messageMock);
        verify(clientMessageToMainSiteService, times(1))
                .handleMessage(playerId,  sessionToken, type, body);
        
    }
    
    @Test
    void handleMessageTest_nullSessionToken() throws Exception {
        var jsonPath = "testing-resources/controller/ClientMessageHandlingControllerTest/MessagePayloadNullSessionToken.json";
        var json = ResourceReader.readAsStr(jsonPath);
        when(messageMock.getPayload()).thenReturn(json);
        
        doThrow(RuntimeException.class)
                .when(clientMessageToMainSiteService)
                .handleMessage(playerId, null, type, body);
    
        
        clientMessageHandlingController.handleMessage(messageMock);
        verify(logger, times(1)).error(anyString(), any(Object.class));
    }
    
    @Test
    void handleMessageTest_handlesWebClientResponseException() throws Exception {
        var jsonPath = "testing-resources/controller/ClientMessageHandlingControllerTest/MessagePayload.json";
        var json = ResourceReader.readAsStr(jsonPath);
        
        when(messageMock.getPayload()).thenReturn(json);
        var exception = new WebClientResponseException("", 400, "testing", null, "".getBytes(StandardCharsets.UTF_8),
                                                       StandardCharsets.UTF_8);
        doThrow(exception)
                .when(clientMessageToMainSiteService)
                .handleMessage(playerId, sessionToken, type, body);
        
        clientMessageHandlingController.handleMessage(messageMock);
        verify(logger, times(1)).error(anyString(), any(Throwable.class));
    }
    
    @Test
    void handleMessageTest_handlesIllegalStateException() throws Exception {
        var jsonPath = "testing-resources/controller/ClientMessageHandlingControllerTest/MessagePayload.json";
        var json = ResourceReader.readAsStr(jsonPath);
        
        when(messageMock.getPayload()).thenReturn(json);
        var exception = new IllegalStateException("");
        doThrow(exception)
                .when(clientMessageToMainSiteService)
                .handleMessage(playerId, sessionToken, type, body);
        
        clientMessageHandlingController.handleMessage(messageMock);
        verify(logger, times(1)).info(anyString());
    }
}