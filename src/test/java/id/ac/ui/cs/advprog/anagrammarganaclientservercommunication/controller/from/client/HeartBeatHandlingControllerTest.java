package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller.from.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.AuthenticationService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.HeartBeatHandlingService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.Message;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class HeartBeatHandlingControllerTest {
    
    @MockBean
    AuthenticationService authenticationService;
    
    @MockBean
    HeartBeatHandlingService heartBeatHandlingService;
    
    
    PlayerIdentity playerIdentity = new PlayerIdentity(123, "MyRoom", "");
    Message<String> msg;
    
    @InjectMocks
    HeartBeatHandlingController heartBeatHandlingController;
    
    
    @SuppressWarnings("unchecked")
    @BeforeEach
    @SneakyThrows
    void setUp() {
        msg = Mockito.mock(Message.class);
        var mapper = new ObjectMapper();
        var playerIdentityJson = mapper.writeValueAsString(playerIdentity);
    
        MockitoAnnotations.openMocks(this);
        when(msg.getPayload()).thenReturn(playerIdentityJson);
    }
    
    
    @Test
    void handleIncomingHeartbeat_runsWellWhenAuthenticated() {
        var playerId = playerIdentity.getPlayerId();
        
        when(authenticationService.authenticateByUsingRepository(playerIdentity)).thenReturn(true);
        heartBeatHandlingController.handleIncomingHeartbeat(msg);
        verify(heartBeatHandlingService, times(1)).refreshOnlineStatus(playerId);
    }
    
    
    @Test
    void handleIncomingHeartbeat_doNotRefreshIfNotAuthenticated() {
        var playerId = playerIdentity.getPlayerId();
        
        when(authenticationService.authenticateByUsingRepository(playerIdentity)).thenReturn(false);
        heartBeatHandlingController.handleIncomingHeartbeat(msg);
        verify(heartBeatHandlingService, times(0)).refreshOnlineStatus(playerId);
    }
    
    @Test
    void handleIncomingHeartbeat_noNeedToHandleError() {
        when(authenticationService.authenticateByUsingRepository(playerIdentity)).thenThrow(RuntimeException.class);
        assertThrows(RuntimeException.class,
                     () -> heartBeatHandlingController.handleIncomingHeartbeat(msg));
    }
}