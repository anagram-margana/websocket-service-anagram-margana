package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.controller.from.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service.AuthenticationService;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.ResourceReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.Message;


import java.io.IOException;

import static org.mockito.Mockito.*;

class InitialConnectionControllerTest {
    
    
    AuthenticationService authenticationService;
    
    @InjectMocks
    InitialConnectionController initialConnectionController;
    
    @BeforeEach
    void setup(){
        authenticationService = Mockito.mock(AuthenticationService.class);
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void handleInitRequestRunsWithoutException() throws IOException {
        var jsonPath = "testing-resources/controller/websocket/InitialConnectionControllerTest/PlayerIdentityMessage.json";
        var json = ResourceReader.readAsStr(jsonPath);
        var objectMapper = new ObjectMapper();
        var playerIdentity = objectMapper.readValue(json, PlayerIdentity.class);
        
        var message = (Message<String>) Mockito.mock(Message.class);
        when(message.getPayload()).thenReturn(json);
        
        when(authenticationService.authenticateToServer(playerIdentity)).thenReturn(true);
        initialConnectionController.handleInitRequest(message);
        verify(authenticationService, Mockito.times(1)).authenticateToServer(playerIdentity);
        clearInvocations(authenticationService);
    
        when(authenticationService.authenticateToServer(playerIdentity)).thenReturn(false);
        initialConnectionController.handleInitRequest(message);
        verify(authenticationService, Mockito.times(1)).authenticateToServer(playerIdentity);
    }
}