package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PlayerIdentityRepositoryTest {
    
    PlayerIdentityRepository playerIdentityRepositoryMock;
    
    String playerSessionToken = "TOKEN";
    
    PlayerIdentity playerIdentity1;
    int playerId1 = 102;
    String playerRoomCode1 = "abc";
    PlayerIdentity playerIdentity2;
    int playerId2 = 103;
    String playerRoomCode2 = "def";
    
    @BeforeEach
    void setup(){
        playerIdentityRepositoryMock = Mockito.mock(PlayerIdentityRepository.class,
                                                    Mockito.withSettings()
                                                            .defaultAnswer(Answers.CALLS_REAL_METHODS));
        playerIdentityRepositoryMock.init();
        
        playerIdentity1 = new PlayerIdentity();
        playerIdentity2 = new PlayerIdentity();
        
        playerIdentity1.setPlayerId(playerId1);
        playerIdentity1.setRoomCode(playerRoomCode1);
        playerIdentity1.setSessionToken(playerSessionToken);
        
        playerIdentity2.setPlayerId(playerId2);
        playerIdentity2.setRoomCode(playerRoomCode2);
        playerIdentity2.setSessionToken(playerSessionToken);
        
        playerIdentityRepositoryMock.add(playerIdentity1);
        playerIdentityRepositoryMock.add(playerIdentity2);
    }
    
    
    @Test
    void findByPlayerIdOrNull() {
        var player1 = playerIdentityRepositoryMock.findByPlayerIdOrNull(playerId1);
        var player2 = playerIdentityRepositoryMock.findByPlayerIdOrNull(playerId2);
        var nullPlayer = playerIdentityRepositoryMock.findByPlayerIdOrNull(12345);
        
        assertSame(playerIdentity1, player1);
        assertSame(playerIdentity2, player2);
        assertNull(nullPlayer);
    }
    
    @Test
    void findAll() {
        var banyaknyaPlayerIdentityYangTersimpan = 2;
        var allPlayerIdentity = new ArrayList<>(playerIdentityRepositoryMock.findAll());
    
        assertSame(banyaknyaPlayerIdentityYangTersimpan, allPlayerIdentity.size());
        assertTrue(allPlayerIdentity.contains(playerIdentity1));
        assertTrue(allPlayerIdentity.contains(playerIdentity2));
    }
}