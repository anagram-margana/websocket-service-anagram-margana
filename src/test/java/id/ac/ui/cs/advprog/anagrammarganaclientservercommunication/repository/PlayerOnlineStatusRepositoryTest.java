package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository;

import com.google.common.cache.RemovalNotification;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.testing.util.AdvanceableTicker;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class PlayerOnlineStatusRepositoryTest {
    @InjectMocks
    PlayerOnlineStatusRepository playerOnlineStatusRepository;
    
    PlayerIdentity player1 = new PlayerIdentity(123, "", "");
    PlayerIdentity player2 = new PlayerIdentity(124, "", "");
    AdvanceableTicker advanceableTicker = new AdvanceableTicker();
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        playerOnlineStatusRepository.init();
        playerOnlineStatusRepository.setTicker(advanceableTicker);
        playerOnlineStatusRepository.setExpirationTimeout(4, TimeUnit.HOURS);
    }
    
    @SneakyThrows
    @Test
    void setExpirationTimeout_throwsIfCacheNotEmpty() {
        assertDoesNotThrow(() -> playerOnlineStatusRepository.setExpirationTimeout(4, TimeUnit.HOURS));
    
        playerOnlineStatusRepository.refreshOnlineStatus(player1);
        assertThrows(IllegalStateException.class,
                () -> playerOnlineStatusRepository.setExpirationTimeout(4, TimeUnit.HOURS));
    
        advanceableTicker.advance(5, TimeUnit.HOURS);
        assertDoesNotThrow(() -> playerOnlineStatusRepository.setExpirationTimeout(4, TimeUnit.HOURS));
    }
    

    @SneakyThrows
    @Test
    void repositoryIsAutomaticallyCleared() {
        playerOnlineStatusRepository.addRemovalListener(this::eventListener);
        
        assertEquals(0, playerOnlineStatusRepository.getSize());
        playerOnlineStatusRepository.refreshOnlineStatus(player1);
        playerOnlineStatusRepository.refreshOnlineStatus(player2);
        assertEquals(2, playerOnlineStatusRepository.getSize());
        assertNotNull(player1);
        assertNotNull(player2);
    
        
        var _235_minutes = 4*60-5;
        advanceableTicker.advance(_235_minutes, TimeUnit.MINUTES);
        playerOnlineStatusRepository.refreshOnlineStatus(player2);
        assertEquals(2, playerOnlineStatusRepository.getSize());
        assertNotNull(player1);
        assertNotNull(player2);
    
        advanceableTicker.advance(_235_minutes, TimeUnit.MINUTES);
        assertEquals(1, playerOnlineStatusRepository.getSize());
        assertNull(player1);
        assertNotNull(player2);
    
        advanceableTicker.advance(_235_minutes, TimeUnit.MINUTES);
        assertEquals(0, playerOnlineStatusRepository.getSize());
        assertNull(player1);
        assertNull(player2);
    }
    
    
    @SneakyThrows
    @Test
    void isOnlineWorksProperly() {
        playerOnlineStatusRepository.setExpirationTimeout(3, TimeUnit.HOURS);
        assertEquals(0, playerOnlineStatusRepository.getSize());
        
        playerOnlineStatusRepository.refreshOnlineStatus(player1);
        assertEquals(1, playerOnlineStatusRepository.getSize());
        assertTrue(playerOnlineStatusRepository.isOnline(player1));
        
        var _175_minutes = 3*60-5;
        advanceableTicker.advance(_175_minutes, TimeUnit.MINUTES);
        playerOnlineStatusRepository.refreshOnlineStatus(player1);
        assertEquals(1, playerOnlineStatusRepository.getSize());
        assertTrue(playerOnlineStatusRepository.isOnline(player1));
        
        advanceableTicker.advance(_175_minutes, TimeUnit.MINUTES);
        assertEquals(1, playerOnlineStatusRepository.getSize());
        assertTrue(playerOnlineStatusRepository.isOnline(player1));
        
        advanceableTicker.advance(_175_minutes, TimeUnit.MINUTES);
        var temp = playerOnlineStatusRepository.getSize();
        var temp2 = playerOnlineStatusRepository.getSize();
        assertEquals(0, temp);
        assertFalse(playerOnlineStatusRepository.isOnline(player1));
    }
    
    private void eventListener(RemovalNotification<Long, PlayerIdentity> notif){
        if (!notif.wasEvicted())
            return;
        
        if (player1 != null && player1.equals(notif.getValue()))
            player1 = null;
        if (player2 != null && player2.equals(notif.getValue()))
            player2 = null;
    }
    
}