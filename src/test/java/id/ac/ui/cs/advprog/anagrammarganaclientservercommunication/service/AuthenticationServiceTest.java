package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSender;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSenderFactory;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.RequestSender;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.slf4j.Logger;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;



class AuthenticationServiceTest {
    @Mock
    PlayerIdentityRepository playerIdentityRepository;
    @InjectMocks
    AuthenticationService authenticationService;
    URL thisWebsocketServiceUrlAddress;
    
    long playerId = 123;
    String roomCode = "my-room";
    String sessionToken = "asdfghjkl";
    PlayerIdentity playerIdentity;
    
    IRequestSender mockedRequestSender;
    IRequestSenderFactory mockedRequestSenderFactory;
    
    
    @BeforeEach
    @SneakyThrows
    void setup(){
        try{
            thisWebsocketServiceUrlAddress = new URL("http://localhost:9091");  // arbitrary
            
            
            playerIdentityRepository = Mockito.mock(PlayerIdentityRepository.class);
            mockedRequestSender = Mockito.mock(RequestSender.class);
            mockedRequestSenderFactory = Mockito.mock(IRequestSenderFactory.class);
    
            MockitoAnnotations.openMocks(this);
    
            when(mockedRequestSenderFactory.createRequestSender(any(URL.class)))
                    .thenReturn(mockedRequestSender);
            when(mockedRequestSenderFactory.createRequestSender(any(URL.class), any(WebClient.class)))
                    .thenReturn(mockedRequestSender);
            authenticationService.init(new URL("Http://localhost:8080"),
                                       mockedRequestSenderFactory,
                                       thisWebsocketServiceUrlAddress);
    
    
            playerIdentity = new PlayerIdentity(playerId, roomCode, sessionToken);
            when(playerIdentityRepository.findByPlayerIdOrNull(playerId)).thenReturn(playerIdentity);
            when(playerIdentityRepository.findByPlayerIdOrNull(not(eq(playerId))))
                    .thenReturn(null);
        }catch (MalformedURLException e){
            throw new IllegalStateException(e);
        }
    }
    
    
    @Test
    void checkIfAuthenticateToServerRunWellOnAllCondition() {
        var logger = Mockito.mock(Logger.class);
        var formData = Map.of(
                "roomCode", roomCode,
                "playerId", Long.toString(playerId),
                "sessionToken", sessionToken,
                "websocketServiceAddress", thisWebsocketServiceUrlAddress.toString()
        );
        
        when(mockedRequestSender.sendPost(formData, String.class)).thenReturn("1");
        assertTrue(authenticationService.authenticateToServer(playerIdentity));
        verify(mockedRequestSender, times(1)).sendPost(formData, String.class);
    
        when(mockedRequestSender.sendPost(formData, String.class)).thenReturn("0");
        assertFalse(authenticationService.authenticateToServer(playerIdentity));
    
        when(mockedRequestSender.sendPost(formData, String.class)).thenReturn("-1");
        assertFalse(authenticationService.authenticateToServer(playerIdentity));
    
        authenticationService.setLogger(logger);
        when(mockedRequestSender.sendPost(formData, String.class)).thenReturn("asdfghjkl");  // invalid return type
        assertFalse(authenticationService.authenticateToServer(playerIdentity));
        verify(logger, times(1)).error(anyString(), any(), any(), any());
    }
    
    
    
    @Test
    void authenticateToServer_doNotHandleException() {
        var formData = Map.of(
                "roomCode", roomCode,
                "playerId", Long.toString(playerId),
                "sessionToken", sessionToken,
                "websocketServiceAddress", thisWebsocketServiceUrlAddress.toString()
        );
        
        var exception = new IllegalStateException("");
        when(mockedRequestSender.sendPost(formData, String.class)).thenThrow(exception);
        assertThrows(IllegalStateException.class,
                     () -> authenticationService.authenticateToServer(playerIdentity));
    }
    
    
    @Test
    void testAuthenticateByUsingRepositoryCorrectlyReturnsTrue(){
        var messageDto = new MessageDTO<>(playerId, roomCode, sessionToken, "", "");
        var messageDtoWithNullRoom = new MessageDTO<>(playerId, null, sessionToken, "", "");
    
        assertTrue(authenticationService.authenticateByUsingRepository(messageDto));
        assertTrue(authenticationService.authenticateByUsingRepository(messageDtoWithNullRoom));
        assertTrue(authenticationService.authenticateByUsingRepository(playerIdentity));
    }
    
    @Test
    void testAuthenticateByUsingRepositoryCorrectlyReturnsFalse(){
        var messageDto = new MessageDTO<>(playerId, roomCode, sessionToken, "", "");
        var messageDtoWithNullRoom = new MessageDTO<>(playerId, null, sessionToken, "", "");
        
        var invalidPlayerId = 9566;
        messageDto.setPlayerId(invalidPlayerId);
        messageDtoWithNullRoom.setPlayerId(invalidPlayerId);
        assertFalse(authenticationService.authenticateByUsingRepository(messageDto));
        assertFalse(authenticationService.authenticateByUsingRepository(messageDtoWithNullRoom));
        
    
        messageDto.setPlayerId(playerId);
        messageDto.setSessionToken(null);
        messageDtoWithNullRoom.setPlayerId(playerId);
        messageDtoWithNullRoom.setSessionToken(null);
        assertFalse(authenticationService.authenticateByUsingRepository(messageDto));
        assertFalse(authenticationService.authenticateByUsingRepository(messageDtoWithNullRoom));
    }
    
    @Test
    void savePlayerIdentityDoSaveTheIdentity(){
        var newIdentity = new PlayerIdentity(12345, "newRoom", "qwertyuiop");
        
        authenticationService.savePlayerIdentity(newIdentity);
        verify(playerIdentityRepository, times(1)).add(newIdentity);
    }
}