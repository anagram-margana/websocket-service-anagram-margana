package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSender;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.IRequestSenderFactory;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util.RequestSender;
import lombok.SneakyThrows;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class ClientMessageToMainSiteServiceTest {
    PlayerIdentityRepository playerIdentityRepository;
    AuthenticationService authenticationService;
    SendMessageToClientService sendMessageToClientService;
    
    @MockBean
    HeartBeatHandlingService heartBeatHandlingService;
    
    @InjectMocks
    ClientMessageToMainSiteService clientMessageToMainSiteService;
    
    long playerId = 107;
    String roomCode = "my-room";
    String sessionToken = "asdfghjkl";
    PlayerIdentity playerIdentity;
    String type = "testing";
    String message = "message body";
    MessageDTO<Object> messageDTO;
    
    URL mainServerAddr;
    
    @SneakyThrows
    @BeforeEach
    void initializePlayerIdentity(){
        playerIdentityRepository = Mockito.mock(PlayerIdentityRepository.class,
                                                withSettings().useConstructor());
        authenticationService = Mockito.mock(AuthenticationService.class,
                                             withSettings().useConstructor());
        sendMessageToClientService = Mockito.mock(SendMessageToClientService.class,
                                                  withSettings().useConstructor());
        MockitoAnnotations.openMocks(this);
        
        playerIdentity = new PlayerIdentity();
        playerIdentity.setPlayerId(playerId);
        playerIdentity.setRoomCode(roomCode);
        playerIdentity.setSessionToken(sessionToken);
        
        messageDTO = new MessageDTO<>();
        messageDTO.setPlayerId(playerId);
        messageDTO.setRoomCode(roomCode);
        messageDTO.setSessionToken(sessionToken);
        messageDTO.setType(type);
        messageDTO.setBody(message);
    
        mainServerAddr = new URL("http://localhost:8080/");
        clientMessageToMainSiteService.setMainServerAddress(mainServerAddr);
        when (playerIdentityRepository.findByPlayerIdOrNull(playerId)).thenReturn(playerIdentity);
    }
    
    
    @Test
    void checkIfHandleMessageRunsWithoutException_LoggedWhenUrlInvalid() {
        var mockedLogger = Mockito.mock(Logger.class);
        when(authenticationService.authenticateByUsingRepository(playerId, sessionToken))
                .thenReturn(true);
        when(authenticationService.authenticateByUsingRepository(playerId, roomCode, sessionToken))
                .thenReturn(true);
        
        clientMessageToMainSiteService.setMainServerAddress(null);
        clientMessageToMainSiteService.setLogger(mockedLogger);
        clientMessageToMainSiteService.handleMessage(playerId, sessionToken, type, message);
        
        verify(mockedLogger, times(1))
                .error(anyString(), any(MalformedURLException.class));
    }
    
    @SneakyThrows
    @Test
    void checkIfHandleMessageRunsWithoutException_ValidIdentity() {
        when(authenticationService.authenticateByUsingRepository(playerId, sessionToken))
                .thenReturn(true);
        when(authenticationService.authenticateByUsingRepository(playerId, roomCode, sessionToken))
                .thenReturn(true);
    
        var messageDTO = new MessageDTO<>();
        var requestSender = initializeRequestSenderMock();
        var mapCaptor = ArgumentCaptor.forClass(Object.class);
        
        when(requestSender.sendPost(any(Object.class),
                                    any(ParameterizedTypeReference.class)))
                .thenReturn(messageDTO);
        
        clientMessageToMainSiteService.handleMessage(playerId, sessionToken, type, message);
        verify(authenticationService, times(1))
                .authenticateByUsingRepository(playerId, sessionToken);
        verify(requestSender, times(1))
                .sendPost(mapCaptor.capture(), any(ParameterizedTypeReference.class));
    
        verifyResultingMap((Map<String, Object>) mapCaptor.getValue());
        verify(sendMessageToClientService, times(1)).sendMessage(messageDTO);
    }
    
    @SneakyThrows
    private IRequestSender initializeRequestSenderMock(){
        var destinationUrl = new URL(mainServerAddr, "/communication-api/msg-from-client/" + type);
        var requestSender = Mockito.mock(IRequestSender.class);
        var requestSenderFactory = Mockito.mock(IRequestSenderFactory.class);
        when(requestSenderFactory.createRequestSender(destinationUrl))
                .thenReturn(requestSender);
        clientMessageToMainSiteService.setRequestSenderFactory(requestSenderFactory);
        
        return requestSender;
    }
    
    private void verifyResultingMap(Map<String, Object> hashmap){
        assertEquals(Long.toString(playerId), hashmap.get("playerId"));
        assertEquals(roomCode, hashmap.get("roomCode"));
        assertEquals(sessionToken, hashmap.get("sessionToken"));
        assertEquals(message, hashmap.get("body"));
    }
    
    
    @Test
    void checkIfHandleMessageRunsThrowsException_InvalidIdentity() {
        var invalidSession = "yohoo";
        when (authenticationService.authenticateByUsingRepository(eq(playerId),
                                                                  not(eq(invalidSession))
        )).thenReturn(false);
        
        assertThrows(IllegalStateException.class,
                     () -> clientMessageToMainSiteService.handleMessage(playerId, invalidSession, type, message));
    }
    
    @Test
    void checkIfHandleMessageRunsThrowsException_UnknownPlayerId() {
        var arbitraryId = 1234124;
        when (authenticationService.authenticateByUsingRepository(anyLong(), anyString())).thenReturn(true);
        
        assertThrows(IllegalStateException.class,
                     () -> clientMessageToMainSiteService.handleMessage(arbitraryId, sessionToken, type, message));
    }
    
    @Test
    void checkIfHandleMessageRunsThrowsException_NullRoomCode() {
        playerIdentity.setRoomCode(null);
        when (authenticationService.authenticateByUsingRepository(anyLong(), anyString())).thenReturn(true);
        
        assertThrows(IllegalStateException.class,
                     () -> clientMessageToMainSiteService.handleMessage(playerId, sessionToken, type, message));
    }
    
   
    
    @Test
    void sendMessageToMainSite() {
        var requestSender = Mockito.mock(RequestSender.class);
        Object requestDataThatShouldBeSent = Map.of(
                "roomCode", roomCode,
                "playerId", Long.toString(playerId),
                "sessionToken", sessionToken,
                "body", message
        );
        var objectMessageDtoTypeReference = new ParameterizedTypeReference<MessageDTO<Object>>(){};
        when(requestSender.sendPost(requestDataThatShouldBeSent, objectMessageDtoTypeReference))
                .thenReturn(messageDTO);
        
        clientMessageToMainSiteService.sendMessageToMainSiteWithRequestSender(playerId, roomCode,
                                                                              sessionToken, message, requestSender);
        
        verify(requestSender, times(1))
                .sendPost(requestDataThatShouldBeSent, objectMessageDtoTypeReference);
    }
    
    @Test
    void replyBackToClient() throws JsonProcessingException {
        clientMessageToMainSiteService.replyBackToClient(messageDTO);
        verify(sendMessageToClientService, times(1)).sendMessage(messageDTO);
    }
    
    
    @Test
    void replyBackToClientWithException() throws JsonProcessingException {
        when(sendMessageToClientService.sendMessage(any())).thenThrow(JsonProcessingException.class);
        clientMessageToMainSiteService.replyBackToClient(messageDTO);
        verify(sendMessageToClientService, times(1)).sendMessage(messageDTO);
    }
    
    @Test
    void testSendMessageToMainSite() throws MalformedURLException {
        var requestSender = Mockito.mock(RequestSender.class);
        Object requestDataThatShouldBeSent = Map.of(
                "roomCode", roomCode,
                "playerId", Long.toString(playerId),
                "sessionToken", sessionToken,
                "body", message
        );
        var objectMessageDtoTypeReference = new ParameterizedTypeReference<MessageDTO<Object>>(){};
        when(requestSender.sendPost(requestDataThatShouldBeSent, objectMessageDtoTypeReference))
                .thenReturn(messageDTO);
    
        var requestSenderFactory = Mockito.mock(IRequestSenderFactory.class);
        when(requestSenderFactory.createRequestSender(any()))
                .thenReturn(requestSender);
        when(requestSenderFactory.createRequestSender(any(), any()))
                .thenReturn(requestSender);
    
        clientMessageToMainSiteService.setRequestSenderFactory(requestSenderFactory);
        clientMessageToMainSiteService.setMainServerAddress(new URL("http://localhost:8080"));  // any arbitrary valid url
        var ret = clientMessageToMainSiteService.sendMessageToMainSite(playerId, roomCode,
                                                                       sessionToken, type, message);
        
        verify(requestSender, times(1))
                .sendPost(requestDataThatShouldBeSent, objectMessageDtoTypeReference);
        assertSame(messageDTO, ret);
    }
}