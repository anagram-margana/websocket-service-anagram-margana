package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.PlayerIdentity;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerOnlineStatusRepository;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.testing.util.AdvanceableTicker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class HeartBeatHandlingServiceTest {
    @MockBean
    PlayerOnlineStatusRepository playerOnlineStatusRepository;
    
    @MockBean
    PlayerIdentityRepository playerIdentityRepository;
    
    @MockBean
    ClientMessageToMainSiteService clientMessageToMainSiteService;
    
    @InjectMocks
    HeartBeatHandlingService heartBeatHandlingService;
    
    long playerId = 123;
    String roomCode = "MyRoom";
    String sessionToken = "my-session";
    PlayerIdentity playerIdentity = new PlayerIdentity(playerId, roomCode, sessionToken);
    
    long playerId2 = 124;
    String roomCode2 = "MyRoom2";
    String sessionToken2 = "my-session2";
    PlayerIdentity playerIdentity2 = new PlayerIdentity(playerId2, roomCode2, sessionToken2);
    
    long playerId3 = 125;
    String roomCode3 = "MyRoom3";
    String sessionToken3 = "my-session3";
    PlayerIdentity playerIdentity3 = new PlayerIdentity(playerId3, roomCode3, sessionToken3);
    
    @BeforeEach
    void setUp() {
        when(playerIdentityRepository.findByPlayerIdOrNull(playerId)).thenReturn(playerIdentity);
        when(playerIdentityRepository.findByPlayerIdOrNull(playerId2)).thenReturn(playerIdentity2);
        when(playerIdentityRepository.findByPlayerIdOrNull(playerId3)).thenReturn(playerIdentity3);
        MockitoAnnotations.openMocks(this);
    }
    
    
    @Test
    void refreshOnlineStatus_doNotSendMessageIfPreviouslyOnline() {
        refreshOnlineStatus(true);
        verify(clientMessageToMainSiteService, times(0))
                .sendMessageToMainSite(playerId, roomCode, sessionToken, "connected", "");
    }
    
    @Test
    void refreshOnlineStatus_sendMessageIfPreviouslyOnline() {
        refreshOnlineStatus(false);
        verify(clientMessageToMainSiteService, times(1))
                .sendMessageToMainSite(playerId, roomCode, sessionToken, "connected", "");
    }
    
    @Test
    void refreshOnlineStatus_playerDisconnectMessageSentSuccessfully() {
        playerOnlineStatusRepository = Mockito.mock(
                PlayerOnlineStatusRepository.class, withSettings()
                        .useConstructor()
                        .defaultAnswer(Answers.CALLS_REAL_METHODS));
        MockitoAnnotations.openMocks(this);

        var advanceableTicker = new AdvanceableTicker();
        playerOnlineStatusRepository.setTicker(advanceableTicker);
        playerOnlineStatusRepository.init();
        heartBeatHandlingService.init();
    
        doReturn(true).when(playerOnlineStatusRepository).isOnline(any());
        heartBeatHandlingService.refreshOnlineStatus(playerId);
        heartBeatHandlingService.refreshOnlineStatus(playerId2);
        
        heartBeatHandlingService.triggerCleanUp();
        verifyDisconnectMessageIsSentForAllPlayer(0, 0, 0);
        
        advanceableTicker.advance(10, TimeUnit.SECONDS);
        heartBeatHandlingService.refreshOnlineStatus(playerId2);
        
        heartBeatHandlingService.triggerCleanUp();
        verifyDisconnectMessageIsSentForAllPlayer(0, 0, 0);
        
        advanceableTicker.advance(2, TimeUnit.SECONDS);
        heartBeatHandlingService.triggerCleanUp();
        verifyDisconnectMessageIsSentForAllPlayer(1, 0, 0);
        
        advanceableTicker.advance(10, TimeUnit.SECONDS);
        heartBeatHandlingService.triggerCleanUp();
        verifyDisconnectMessageIsSentForAllPlayer(1, 1, 0);
    }
    
    private void verifyDisconnectMessageIsSentForAllPlayer(int numberOfTimesPlayer1, int numberOfTimesPlayer2,
                                                           int numberOfTimesPlayer3){
        verifyDisconnectMessageIsSent(playerId, roomCode, sessionToken, numberOfTimesPlayer1);
        verifyDisconnectMessageIsSent(playerId2, roomCode2, sessionToken2, numberOfTimesPlayer2);
        verifyDisconnectMessageIsSent(playerId3, roomCode3, sessionToken3, numberOfTimesPlayer3);
        
    }
    
    private void verifyDisconnectMessageIsSent(long playerId, String roomCode, String sessionToken, int numberOfTimes){
        verify(clientMessageToMainSiteService, times(numberOfTimes))
                .sendMessageToMainSite(playerId, roomCode, sessionToken, "disconnected", "");
        
    }

    
    void refreshOnlineStatus(boolean onlineStatus){
        var playerId = playerIdentity.getPlayerId();
        when(playerOnlineStatusRepository.isOnline(playerIdentity)).thenReturn(onlineStatus);
        heartBeatHandlingService.refreshOnlineStatus(playerId);
        
        verify(playerIdentityRepository, times(1)).findByPlayerIdOrNull(playerId);
        verify(playerOnlineStatusRepository, times(1)).refreshOnlineStatus(playerIdentity);
    }
}