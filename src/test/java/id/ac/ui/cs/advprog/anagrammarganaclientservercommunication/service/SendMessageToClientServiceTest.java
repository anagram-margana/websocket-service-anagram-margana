package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.repository.PlayerIdentityRepository;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SendMessageToClientServiceTest {
    @LocalServerPort
    private int port;
    private String URL;
    
    @Autowired
    @InjectMocks
    SendMessageToClientService sendMessageToClientService;
    
    @SpyBean
    SimpMessagingTemplate messagingTemplate;
    
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    PlayerIdentityRepository playerIdentityRepository;
    
    CompletableFuture<MessageDtoForClient> receivedWebsocketMessage;
    int playerId;
    String sessionToken;
    MessageDTO<String> formDataDTO;
    
    
 
    
    @BeforeEach
    public void initializeURL(){
        playerIdentityRepository = Mockito.mock(PlayerIdentityRepository.class,
                                                Mockito.withSettings()
                                                        .useConstructor()
                                                        .defaultAnswer(Answers.CALLS_REAL_METHODS));
        MockitoAnnotations.openMocks(this);
        
        
        receivedWebsocketMessage = new CompletableFuture<>();
        URL = "http://localhost:" + port + "/anagram-margana-websocket";
    
        playerId = 13;
        sessionToken = "Tokenasdfghjkl";
    
        formDataDTO = new MessageDTO<>();
        formDataDTO.setPlayerId(playerId);
        formDataDTO.setType("test-mockito");
        formDataDTO.setRoomCode("ABC");
        formDataDTO.setSessionToken(sessionToken);
        formDataDTO.setBody("idk");
    }
    
    
    @Test
    void handleMessageTest() throws Exception {
        subscribeWebsocket(receivedWebsocketMessage);
        sendMessageToClientService.sendMessage(formDataDTO);
        
        var result = receivedWebsocketMessage.get(14, SECONDS);
        System.out.println(result);
        
        assertEquals(formDataDTO.getType(), result.getType());
        assertEquals(formDataDTO.getBody(), result.getBody());
    }
   
    
    
    
    void subscribeWebsocket(CompletableFuture<MessageDtoForClient> receivedWebsocketMessage)
            throws ExecutionException, InterruptedException, TimeoutException {
        // initialize websocket
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new StringMessageConverter());
    
        // connect and subscribe websocket
        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {}).get(2, SECONDS);
        var subscribeAddr = "/subscribe/" +playerId+ "/" +sessionToken;
        stompSession.subscribe(subscribeAddr,
                               new ClientMessageReceiverHandler(receivedWebsocketMessage));
    }
    
    
    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }
    
    
    
    /**
     * These two classes below will act as if they're in client's computer
     */
    private class ClientMessageReceiverHandler implements StompFrameHandler {
        CompletableFuture<MessageDtoForClient> receivedWebsocketMessage;
        
        public ClientMessageReceiverHandler(CompletableFuture<MessageDtoForClient> receivedWebsocketMessage){
            this.receivedWebsocketMessage = receivedWebsocketMessage;
        }
        
        @Override
        public Type getPayloadType(StompHeaders headers) {
            return String.class;
        }
    
        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
            var strPayload = (String) payload;
            var objectMapper = new ObjectMapper();
            MessageDtoForClient object = null;
            try {
                object = objectMapper.readValue(strPayload, MessageDtoForClient.class);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                receivedWebsocketMessage.cancel(true);
            }
            receivedWebsocketMessage.complete(object);
        }
    }
    
    @Data
    @NoArgsConstructor
    public static class MessageDtoForClient{
        String type;
        Object body;
    }
}