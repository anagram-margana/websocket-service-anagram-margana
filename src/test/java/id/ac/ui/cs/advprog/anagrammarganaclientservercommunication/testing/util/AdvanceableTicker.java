package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.testing.util;

import com.google.common.base.Ticker;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class AdvanceableTicker extends Ticker {
    
    private final AtomicLong nanos = new AtomicLong();
    
    /** Advances the ticker value by {@code time} in {@code timeUnit}. */
    public AdvanceableTicker advance(long time, TimeUnit timeUnit) {
        nanos.addAndGet(timeUnit.toNanos(time));
        return this;
    }
    
    @Override
    public long read() {
        long value = nanos.getAndAdd(0);
        return value;
    }
}