package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class JsonParsingUtilityTest {
    JsonParsingUtility jsonParsingUtility;
    
    @BeforeEach
    void createMock(){
        jsonParsingUtility = Mockito.mock(JsonParsingUtility.class, Mockito.withSettings()
                .defaultAnswer(Answers.CALLS_REAL_METHODS)
                .useConstructor());
    }
    
    
    @Test
    void jsonAsHashmapOfString() throws IOException {
        var email = "chongyun@gmail.com";
        var password = "aoT3";
        var username = "chongyun123";
        var level = "23";
        var coin = "12470";
        
        var jsonPath = "testing-resources/util/jsonParsingUtilityTest/jsonWithStringValues.json";
        var jsonString = ResourceReader.readAsStr(jsonPath);
    
        // seharusnya return Hashmap<String, String>
        var resultingHashmap = jsonParsingUtility.<String>jsonAsHashmap(jsonString);
    
        // seharusnya tidak compile time error
        String hashmapEmail = resultingHashmap.get("email");
        hashmapEmail = hashmapEmail.toLowerCase();
        
        assertEquals(email, resultingHashmap.get("email"));
        assertEquals(password, resultingHashmap.get("password"));
        assertEquals(username, resultingHashmap.get("username"));
        assertEquals(level, resultingHashmap.get("level"));
        assertEquals(coin, resultingHashmap.get("coin"));
    }
    
    
    
    @Test
    void jsonAsHashmapOfInteger() throws IOException {
        var level = 23;
        var coin = 12470;
        
        var jsonPath = "testing-resources/util/jsonParsingUtilityTest/jsonWithIntegerValues.json";
        var jsonString = ResourceReader.readAsStr(jsonPath);
    
        // seharusnya return Hashmap<String, Integer>
        var resultingHashmap = jsonParsingUtility.<Integer>jsonAsHashmap(jsonString);
    
        // seharusnya tidak compile time error
        int hashmapEmail = resultingHashmap.get("level");
        hashmapEmail += 5;
        
        assertEquals(level, resultingHashmap.get("level"));
        assertEquals(coin, resultingHashmap.get("coin"));
    }
    
    @Test
    void jsonAsHashmapOfObject() throws IOException {
        var email = "chongyun@gmail.com";
        var password = "aoT3";
        var username = "chongyun123";
        var level = 23;
        var coin = 12470;
        
        var jsonPath = "testing-resources/util/jsonParsingUtilityTest/jsonWithMultipleValueType.json";
        var classLoader = getClass().getClassLoader();
        var jsonStream = classLoader.getResourceAsStream(jsonPath);
        var jsonString = new String(jsonStream.readAllBytes());
    
        // seharusnya return Hashmap<String, Object>
        var resultingHashmap = jsonParsingUtility.jsonAsHashmap(jsonString);
        
        var hashmapEmail = resultingHashmap.get("email");
        var hashmapPassword = resultingHashmap.get("password");
        var hashmapUsername = resultingHashmap.get("username");
        var hashmapLevel = resultingHashmap.get("level");
        var hashmapCoin = resultingHashmap.get("coin");
        
        assertEquals(email, hashmapEmail);
        assertEquals(password, hashmapPassword);
        assertEquals(username, hashmapUsername);
        assertEquals(level, hashmapLevel);
        assertEquals(coin, hashmapCoin);
        
        assertTrue(hashmapEmail instanceof String);
        assertTrue(hashmapPassword instanceof String);
        assertTrue(hashmapUsername instanceof String);
        assertTrue(hashmapLevel instanceof Integer);
        assertTrue(hashmapCoin instanceof Integer);
    }
    
}