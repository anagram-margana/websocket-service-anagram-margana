package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class RequestSenderFactoryTest {
    
    URL url;
    RequestSenderFactory factory;
    
    
    @BeforeEach
    void setup(){
        try {
            url = new URL("http://localhost:8080");
            factory = new RequestSenderFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @Test
    void createRequestSender_urlOnly() {
        var result = factory.createRequestSender(url);
        assertEquals(url, result.getDestination());
    }
    
    @Test
    void testCreateRequestSender() {
        var webclient = WebClient.create(url.toString());
        var result = factory.createRequestSender(url, webclient);
        assertEquals(url, result.getDestination());
    }
}