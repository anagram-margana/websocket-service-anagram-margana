package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.model.MessageDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RequestSenderTest {
    WebClient webClient;
    RequestSender requestSender;
    URL url;
    Map<String, String> formData = Map.of(
            "dummy_key", "dummy value"
    );
    Object objectData;
    
    
    @BeforeEach
    void mockingWebclient() throws MalformedURLException {
        url = new URL("http://some-url.com");
        objectData = new MessageDTO<List<String>>(100, "room", "session", "testing",
                                                  List.of("a", "b"));
    }
    
    @Test
    void requestSenderInstantiationStoresCorrectUrl(){
        var requestSender = new RequestSender(url);
        assertEquals(url, requestSender.getDestination());
    }
    
    
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Test
    void sendPostNonGenericReturnType_formData() throws IOException {
        var nama = "valordra";
        var xp = 5470;
        
        var jsonPath = "testing-resources/util/RequestSenderTest/jsonWithNonGenericType.json";
        initialize(jsonPath);
        
        var result = requestSender.sendPost(formData, DataClassForTesting.class);
        assertEquals(xp, result.xp);
        assertEquals(nama, result.nama);
    }
    
    
    
    @Test
    void sendPostWithGenericReturnType_formData() throws IOException {
        var nama = "valordra";
        var xp = 5470;
        var isiList = "halo";
    
        var jsonPath = "testing-resources/util/RequestSenderTest/jsonWithGenericType.json";
        initialize(jsonPath);
        
        var typeReference = new ParameterizedTypeReference<GenericDataClass<List<String>>>(){};
        var result = requestSender.sendPost(formData, typeReference);
    
        assertEquals(xp, result.xp);
        assertEquals(nama, result.nama);
        assertEquals(isiList, result.body.get(0));
    }
    
    
    
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Test
    void sendPostNonGenericReturnType_objectData() throws IOException {
        var nama = "valordra";
        var xp = 5470;
        
        var jsonPath = "testing-resources/util/RequestSenderTest/jsonWithNonGenericType.json";
        initialize(jsonPath);
        
        var result = requestSender.sendPost(objectData, DataClassForTesting.class);
        assertEquals(xp, result.xp);
        assertEquals(nama, result.nama);
    }
    
    
    
    @Test
    void sendPostWithGenericReturnType_objectData() throws IOException {
        var nama = "valordra";
        var xp = 5470;
        var isiList = "halo";
        
        var jsonPath = "testing-resources/util/RequestSenderTest/jsonWithGenericType.json";
        initialize(jsonPath);
        
        var typeReference = new ParameterizedTypeReference<GenericDataClass<List<String>>>(){};
        var result = requestSender.sendPost(objectData, typeReference);
        
        assertEquals(xp, result.xp);
        assertEquals(nama, result.nama);
        assertEquals(isiList, result.body.get(0));
    }
    
    
    
    public void initialize(String jsonPath) throws IOException {
        var json = ResourceReader.readAsStr(jsonPath);
        
        webClient = WebClient.builder()
                .exchangeFunction(clientRequest -> {
                    return getJsonMonoJust(json);
                }).build();
        
        requestSender = Mockito.mock(RequestSender.class,
                                     Mockito.withSettings()
                                             .useConstructor(url, webClient)
                                             .defaultAnswer(Answers.CALLS_REAL_METHODS));
        
    }
    
    
    public Mono<ClientResponse> getJsonMonoJust(String jsonContent){
        return Mono.just(ClientResponse.create(HttpStatus.OK)
                                 .header("content-type", "application/json")
                                 .body(jsonContent)
                                 .build());
    }
    
    
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static
    class DataClassForTesting{
        String nama;
        int xp;
    }
    
    
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static
    class GenericDataClass<T>{
        String nama;
        int xp;
        T body;
    }
}