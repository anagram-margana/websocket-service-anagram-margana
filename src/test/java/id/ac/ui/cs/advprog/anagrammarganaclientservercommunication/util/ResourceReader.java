package id.ac.ui.cs.advprog.anagrammarganaclientservercommunication.util;

import java.io.IOException;

public class ResourceReader {
    public static String readAsStr(String path) throws IOException {
        var classLoader = ResourceReader.class.getClassLoader();
        var stream = classLoader.getResourceAsStream(path);
        return  new String(stream.readAllBytes());
    }
}
